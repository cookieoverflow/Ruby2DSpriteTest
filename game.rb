require 'ruby2d'
require './player'

set width: 1200, height: 720

@mode = 1
@title = Text.new("Animated sprites", x: 20, y: 20, size: 20, z: 1, color: 'green')
@text = Text.new( "FPS: #{get :fps}", x: 20, y: 50, size: 20, z: 1, color: 'green')

@sprites = []

on :key_down do |event|
  if event.key == '1'
    @mode = 1
    @title.text = "Animated sprites"
    reset
  elsif event.key == '2'
    @mode = 2
    @title.text = "Animated sprites with movement"
    reset
  elsif event.key == '3'
    @mode=3
    @title.text = "Animated sprites with movement and collision"
    reset
  end
end

on :key_held do |event|
  10.times { spawn_sprite } if event.key == 'space'
end

tick = 0

update do
  tick += 1

  if tick % 60 == 0
    @text.text = "FPS: #{get(:fps).to_i}, Sprites: #{@sprites.count}"
  end

  move if @mode == 2

  if @mode == 3
    move
    collide
  end
end

def reset
  @sprites.each { |sprite| Window.remove(sprite.sprite) }
  @sprites = []
end

def spawn_sprite
  x = rand(20..1180)
  y = rand(80..700)

  sprite = Sprite.new(
    'assets/sprite.png',
    x: x, y: y,
    clip_width: 32, clip_height: 32,
    clip_x: 0, clip_y: 96,
    width: 32, height: 32,
    time: 100,
    loop: true,
  )

  player = Player.new(
    sprite: sprite,
    x: x, y: y,
    x_vel: rand(3..10), y_vel: rand(3..10),
    x_dir: [-1, 1].shuffle.first, y_dir: [-1, 1].shuffle.first
  )

  flip = player.direction.x == -1 ? :horizontal : nil
  player.sprite.play flip: flip
  @sprites << player
end

def move
  delta = 10/get(:fps)
  @sprites.each { |sprite| sprite.update(delta) }
end

def collide
  @sprites.each do |sprite1|
    @sprites.each do |sprite2|
      if collided?(sprite1, sprite2)
        sprite1.direction.x *= -1
      end
    end
  end
end

def collided?(s1, s2)
  s1.position.x < s2.position.x + s2.sprite.width &&
  s1.position.x + s1.sprite.width > s2.position.x &&
  s1.position.y < s2.position.y + s2.sprite.height &&
  s1.position.y + s1.sprite.height > s2.position.y
end

show
