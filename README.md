# Ruby2D Sprite Test

This repo is a test to see how many sprites the [Ruby2D](https://www.ruby2d.com/) framework can render.

### Run

To run the test, first download the repo, then:

```ruby
bundle install

ruby game.rb
```

### Usage

There are 3 mode which can be changed by the `1`,`2`,`3` keys.

1. Just render animated sprites.
2. A Render animated sprites with movement logic.
3. Render animated sprites with movement and collision logic.

Note: The collision logic is very inefficient, it's a nested loop to check if every sprite is colliding with every other sprite per frame. I may change this to use ray-tracing in the future but it serves my purposes for now.

To render sprites, hold `space`.

### Credits

The sprite sheet was taken from [@dbhvk on OpenGameArt](https://opengameart.org/content/character-sprite-walk-animation), many thanks!!
