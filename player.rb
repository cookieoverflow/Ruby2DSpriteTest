class Player
  attr_accessor :position, :speed, :sprite, :direction

  def initialize(sprite:,x:, y:, x_vel:, y_vel:, x_dir:, y_dir:)
    @sprite = sprite
    @position = Point.new(x, y)
    @speed = Point.new(x_vel, y_vel)
    @direction = Point.new(x_dir, y_dir)
  end

  def update(delta)
    position.x += speed.x * direction.x * delta
    position.y += speed.y * direction.y * delta
    check_bounds
    sprite.x = position.x
    sprite.y = position.y
  end

  private

  def check_bounds
    if position.x < 0
      direction.x *= -1
      position.x = 0
      sprite.flip_sprite(nil)
    elsif position.x + sprite.width > Window.get(:width)
      direction.x *= -1
      position.x = Window.get(:width) - sprite.width
      sprite.flip_sprite(:horizontal)
    elsif position.y < 80
      position.y = 80
      direction.y *= -1
    elsif position.y + sprite.height > Window.get(:height)
      position.y = Window.get(:height) - sprite.height
      direction.y *= -1
    end
  end
end

Point = Struct.new(:x, :y)
